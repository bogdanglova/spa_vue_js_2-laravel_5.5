/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
window.VueRouter = require('vue-router').default;
window.VeeValidate = require('vee-validate').default;
Vue.use(VueRouter);
Vue.use(VeeValidate);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('add-user-component', require('./components/addNewUser.vue'));
Vue.component('register-component', require('./components/register.vue'));
Vue.component('login-component', require('./components/login.vue'));
Vue.component('invites-component', require('./components/invites.vue'));
Vue.component('index-component', require('./components/index.vue'));
const Invite = {template: '<add-user-component></add-user-component>'};
const Invites = {template: '<invites-component></invites-component>'};
const Register = {template: '<register-component></register-component>'};
const Login = {template: '<login-component></login-component>'};
const Index = {template: '<index-component></index-component>'};
const routes = [
    {path: '/add-invite', component: Invite},
    {path: '/invites', component: Invites},
    {path: '/user/:id', component: Register},
    {path: '/login', component: Login},
    {path: '/', component: Index}
];
const router = new VueRouter({
    routes
});

const app = new Vue({
    // el: '#app',
    router
}).$mount('#app');
