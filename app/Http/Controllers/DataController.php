<?php

namespace App\Http\Controllers;

use App\Mail\MailClass;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DataController extends Controller
{
    public function add(Request $request)
    {
        if ($request->username == null or $request->email == null or $request->group == null) {
            return;
        } else {
            $user = User::create([
                'username' => $request->username,
                'email' => $request->email,
                'group' => $request->group
            ]);
            $link = 'http://project.dev/#/user/' . $user->id;
            Mail::to($user->email)->send(new MailClass($link));
            return $user->id;
        }
    }

    public function register(Request $request)
    {
        if ($request->password == null) {
            return '';
        }
        $user = User::find($request->id);
        $user->password = bcrypt($request->password);
        $user->active = 1;
        $user->save();
        Auth::login($user);
        return redirect()->to('/');
    }

    public function login(Request $request)
    {
        $user = User::where(['email' => $request->email])->first();
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            Auth::login($user);
        } else {
            return 'false';
        }
    }

    public function allInvites()
    {
        $users = User::all();
        return $users;
    }

    public function checkUser()
    {
        if (Auth::check()) {
            $user = User::find(Auth::user()->id);
            return $user->group;
        } else {
            return 'false';
        }
    }

    public function deleteUser(Request $request)
    {
        $user = User::find($request->id);
        $user->delete();
    }
}
